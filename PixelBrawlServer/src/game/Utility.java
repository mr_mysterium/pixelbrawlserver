package game;

import java.util.Random;

import app.PixelBrawlServer;

public class Utility {
private static Random random= new Random();
 
 public static boolean roll(float chance) {
	 float limit = chance/100f;
	 float roll= random.nextFloat();
	 PixelBrawlServer.server.log("Roll: "+roll+" "+"Limit: "+limit+" "+String.valueOf(roll<limit));
	 if (roll<limit)return true;
	 return false;
 };
}
