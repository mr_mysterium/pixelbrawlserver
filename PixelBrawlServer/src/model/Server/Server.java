package model.Server;


import java.awt.Color;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.print.attribute.AttributeSet;
import javax.swing.JScrollBar;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import app.PixelBrawlServer;
import model.Player.CommandHandler;
import model.Player.Match;
import model.Player.Player;

public class Server extends WebSocketServer {
	
	private CommandHandler commandhandler= new CommandHandler();
	private HashMap<WebSocket, Player>playerlist= new HashMap<WebSocket, Player>();
	private HashMap<Player,Match>matchlist= new HashMap<Player,Match>();
	public Server(int port)throws UnknownHostException {
		super(new InetSocketAddress(port));
	}
	public Server(InetSocketAddress address) {
		super(address);
	}


@Override
public void onClose(WebSocket conn, int code, String reason, boolean remote) {
	log(playerlist.get(conn).getUsername()+" left the server");
	matchlist.remove(playerlist.get(conn));
	playerlist.remove(conn);
	listupdate();
	commandhandler.broacastmatchlist();
}

@Override
public void onError(WebSocket conn, Exception e) {
	
}

@Override
public void onMessage(WebSocket conn, String message) {
		log(playerlist.get(conn)+": "+message);
		commandhandler.execute(conn, message);
	
}

@Override
public void onOpen(WebSocket conn, ClientHandshake handshake) {
	Player player = new Player(conn);
	playerlist.put(conn,player);
	listupdate();
	log(player.getUsername()+" joined the server");
}

@Override
public void onStart() {
	log("Server started");
}
public CommandHandler getCommandhandler() {
	return commandhandler;
}

public HashMap<Player,Match>getMatchlist(){
	return matchlist;
}

public HashMap<WebSocket, Player> getPlayerlist() {
	return playerlist;
}
public void log(String s,Color c) {
	Document d= PixelBrawlServer.frame.getDoc();
	StyleContext sc = StyleContext.getDefaultStyleContext();
    javax.swing.text.AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground,c);
	try {
		d.insertString(d.getLength(),s+"\n",aset);
	} catch (BadLocationException e) {
		e.printStackTrace();
	}
}
public void listupdate() {
	PixelBrawlServer.frame.getPlayerlistmodel().clear();
	PixelBrawlServer.frame.getMatchlistmodel().clear();
	playerlist.forEach((k,v)->PixelBrawlServer.frame.getPlayerlistmodel().addElement(v));
	matchlist.forEach((k,v)->PixelBrawlServer.frame.getMatchlistmodel().addElement(v));
}

public void log(String s) {
	try {
		Document d= PixelBrawlServer.frame.getDoc();
		d.insertString(d.getLength(),s+"\n",null);
		JScrollBar vertical = PixelBrawlServer.frame.getScrollPane().getVerticalScrollBar();
		vertical.setValue( vertical.getMaximum() );
	} catch (BadLocationException e) {
		e.printStackTrace();
	}
}
}
